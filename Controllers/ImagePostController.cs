using Microsoft.AspNetCore.Mvc;

public class ImagePostController : Controller
{
    private readonly IWebHostEnvironment _hostingEnvironment;

    private static List<ImagePost> images = new List<ImagePost>();

    public ImagePostController(IWebHostEnvironment hostingEnvironment)
    {
        _hostingEnvironment = hostingEnvironment;
    }

    [HttpGet]
    public ActionResult Index()
    {
        return View(images);
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Create(ImagePost model)
    {
        if (model.Image != null && model.Image.Length > 0)
        {
            model.FileName = Path.GetFileName(model.Image.FileName);
            model.FilePath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads", model.FileName);

            using (var stream = new FileStream( model.FilePath!, FileMode.Create))
            {
                await model.Image.CopyToAsync(stream);
            }

            images.Add(model);

            return RedirectToAction("Index");
        }

        return BadRequest("Nenhum arquivo foi enviado.");
    }
}
