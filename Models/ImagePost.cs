public class ImagePost
{
   public string Legenda { set;get; }
   public IFormFile Image { set; get; }
   public string? FilePath { get; set; }
   public string? FileName { get; set; }
}